package com.wavelabs

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
/**
  * Created by muralikrishnak on 21-Sep-17.
  */
object Hdfs {
def writeToHdfs(uri: String, filePath: String) {
    println("Trying to write to HDFS...")
    val conf = new Configuration()
    conf.set("fs.defaultFS", uri)
    val fs = FileSystem.get(conf)
    val os = fs.append(new Path(filePath))
    println("writing")
    os.write("{ \"device_id\": \"28472834\", \"location\": { \"latitude\": \"17.386062\",\"longitude\": \"78.485180\" } }\n".getBytes())
    println("Done!")
  }

  def main(args: Array[String]): Unit = {
    writeToHdfs("hdfs://localhost:50070", "mk/appsplayuser.txt");
  }
}
