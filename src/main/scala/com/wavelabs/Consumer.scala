package com.wavelabs

/**
  * Created by muralikrishnak on 21-Sep-17.
  */
object Consumer extends App{
  import java.util

  import org.apache.kafka.clients.consumer.KafkaConsumer

  import scala.collection.JavaConverters._

  import java.util.Properties

  val TOPIC="wavelabs"

  val  props = new Properties()
  props.put("bootstrap.servers", "localhost:9200")

  props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
  props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
  props.put("group.id", "2")


  val consumer = new KafkaConsumer[String, String](props)

  consumer.subscribe(util.Collections.singletonList(TOPIC))
  while(true){
    val records=consumer.poll(10000)
    for (record<-records.asScala){
      println(record)
    }
  }
}