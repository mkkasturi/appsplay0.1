name := "Appsplay123"

version := "0.1"

scalaVersion := "2.10.5"

libraryDependencies += "org.apache.hadoop" % "hadoop-client" % "2.2.0"
// https://mvnrepository.com/artifact/org.apache.kafka/kafka_2.10
libraryDependencies += "org.apache.kafka" % "kafka_2.10" % "0.8.0"
// https://mvnrepository.com/artifact/org.apache.kafka/kafka-clients
libraryDependencies += "org.apache.kafka" % "kafka-clients" % "0.10.2.1"